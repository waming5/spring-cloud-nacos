package com.waming.consumer.ws;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.waming.api.IStudentApi;
import com.waming.api.entity.Student;

@RestController
@RequestMapping("face/mall/student")
public class ConsumerController{
	@Autowired
	private IStudentApi studentApi;
	
    @GetMapping("/list")
	public List<Student> list() {
		return studentApi.list();
	}

}
