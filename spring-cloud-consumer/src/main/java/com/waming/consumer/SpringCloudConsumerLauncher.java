package com.waming.consumer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages ={"com.waming"})
@EnableFeignClients({"com.waming"})
@EnableDiscoveryClient
public class SpringCloudConsumerLauncher {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudConsumerLauncher.class, args);
    }

}
