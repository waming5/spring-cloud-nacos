# spring-cloud-nacos

#### 介绍
spring cloud+fegin+nacos 服务近实时下线，解决平滑下线，根据版本支持灰度，仅是一个简单思路。

服务下线向nacos注销服务并关闭nacos客户端，nacos服务端摘掉服务同步推送nacos客服端订阅的服务更新服务列表，负载采用同步的服务列表做轮训算法

#### 软件架构
软件架构说明

spring-cloud-ards  服务平滑下线，提供nacos服务列表进行负载算法。

spring-cloud-api  接口约束

spring-cloud-consumer  消费者

spring-cloud-provider 生产者

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
