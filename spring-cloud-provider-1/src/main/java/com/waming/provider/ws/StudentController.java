package com.waming.provider.ws;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.waming.api.IStudentApi;
import com.waming.api.entity.Student;

@RestController
@RequestMapping("mall/student")
public class StudentController implements IStudentApi{

	@Override
	public List<Student> list() {
		List<Student> students=new ArrayList<Student>();
		Student s1=new Student();
		s1.setId(1L);
		s1.setAge(25);
		s1.setClz("一班");
		s1.setNameCn("李四");
		s1.setNumber("0001");
		Student s2=new Student();
		s2.setId(11L);
		s2.setAge(26);
		s2.setClz("一班");
		s2.setNameCn("张四海");
		s2.setNumber("0002");
		students.add(s1);
		students.add(s2);
		return students;
	}

}
