package com.waming.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients({"com.waming"})
@SpringBootApplication(scanBasePackages ={"com.waming"})
@EnableDiscoveryClient
public class SpringCloudProviderLauncher {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudProviderLauncher.class, args);
    }

}
