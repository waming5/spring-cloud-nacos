package com.waming.api;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.waming.api.entity.Student;

@FeignClient(value = "spring-cloud-provider", contextId = "student", path = "mall/student")
public interface IStudentApi {
	/**
	 * 返回全部学生列表
	 * @return
	 */
    @GetMapping("/list")
	public List<Student> list();
}
