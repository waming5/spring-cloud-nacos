package com.waming.api.entity;

import java.io.Serializable;

public class Student implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * 自增ID
	 */
	private Long id;
	
	/**
	 * 姓名
	 */
	private String nameCn;
	
	/**
	 * 年龄
	 */
	private Integer age;
	
	/**
	 * 学号
	 */
	private String number;
	
	/**
	 * 班级
	 */
	private String clz;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getNameCn() {
		return nameCn;
	}
	public void setNameCn(String nameCn) {
		this.nameCn = nameCn;
	}

	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}

	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}

	public String getClz() {
		return clz;
	}
	public void setClz(String clz) {
		this.clz = clz;
	}
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", nameCn=" + nameCn + ", age=" + age + ", number=" + number + ", clz=" + clz
				+ "]";
	}

}
