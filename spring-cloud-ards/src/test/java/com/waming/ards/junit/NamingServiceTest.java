package com.waming.ards.junit;

import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.waming.ards.balance.NacosFinalBalancerRule;

public class NamingServiceTest {
	private static Logger logger=LogManager.getLogger(NacosFinalBalancerRule.class);

	public void servicelist() {
		Properties properties = new Properties();
		properties.setProperty("serverAddr","nacos.dev.hosemall.cn:80");
		properties.setProperty("namespace","c8c83e4d-95df-4df5-a7ed-8aca04699892");
		NamingService naming =null;
        String serviceName ="spring-cloud-provider";
        String groupName ="DEFAULT_GROUP";
        String clusterName ="DEFAULT";
        String ip ="192.168.3.32";
        int port = 8081;
		try {
			naming = NamingFactory.createNamingService(properties);
			List<Instance> instances=naming.getAllInstances(serviceName);
			for(Instance in:instances) {
				System.out.println(in);
			}
		} catch (NacosException e) {
			e.printStackTrace();
		}
        try {
            naming.registerInstance(serviceName, groupName, ip, port, clusterName);
            logger.info("registerInstance from nacos, serviceName:{}, groupName:{}, clusterName:{}, ip:{}, port:{}", serviceName, groupName, clusterName, ip, port);
            Thread.sleep(10000);
        	naming.deregisterInstance(serviceName, groupName, ip, port, clusterName);
            logger.info("deregister from nacos, serviceName:{}, groupName:{}, clusterName:{}, ip:{}, port:{}", serviceName, groupName, clusterName, ip, port);
        } catch (NacosException e) {
        	logger.error("deregister from nacos error", e);
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 后下线
		logger.info("程序开始停止");
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
