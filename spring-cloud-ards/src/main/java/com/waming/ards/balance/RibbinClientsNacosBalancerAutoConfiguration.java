package com.waming.ards.balance;

import org.springframework.cloud.client.ConditionalOnDiscoveryEnabled;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Configuration;
import com.alibaba.cloud.nacos.ConditionalOnNacosDiscoveryEnabled;

@Configuration(proxyBeanMethods = false)
@ConditionalOnDiscoveryEnabled
@ConditionalOnNacosDiscoveryEnabled
@RibbonClients(defaultConfiguration=NacosFinalBalancerRule.class)
public class RibbinClientsNacosBalancerAutoConfiguration {

}
