package com.waming.ards.balance;

public interface DiscoveryConstant {
	 /**依赖服务目标版本**/
	 String TARGET_VERSION="target-version";
	 /**依赖服务实例版本**/
	 String VERSION="version";
}
